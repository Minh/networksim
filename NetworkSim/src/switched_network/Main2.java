/*
 *  (c) K.Bryson, Dept. of Computer Science, UCL (2013)
 */

package switched_network;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This simulates a computer network consisting of two computers
 * with server application running on one machine and a
 * client application running on the other machine.
 *
 * The code should work with this setup ... although it should
 * also work with different network structures ...
 * (so 10 computers each running 10 applications!)
 *
 * @author K. Bryson.
 */
public class Main2 {


    public static void main(String[] args) {

        try {

            // Create 2 computers named 'A' and 'B' with specific IP addresses.
            Computer computerA = new Computer("A", InetAddress.getByName("1.2.3.1"));
            Computer computerB = new Computer("B", InetAddress.getByName("1.2.3.2"));
            Computer computerC = new Computer("C", InetAddress.getByName("1.2.3.3"));
            Computer computerD = new Computer("D", InetAddress.getByName("1.2.3.4"));
            Computer computerE = new Computer("E", InetAddress.getByName("1.2.3.5"));
            Computer computerF = new Computer("F", InetAddress.getByName("1.2.3.6"));
            Computer computerG = new Computer("G", InetAddress.getByName("1.2.3.7"));
            Computer computerH = new Computer("H", InetAddress.getByName("1.2.3.8"));
            Computer computerI = new Computer("I", InetAddress.getByName("1.2.3.9"));
            Computer computerJ = new Computer("J", InetAddress.getByName("1.2.3.10"));
            Computer computerK = new Computer("K", InetAddress.getByName("1.2.3.11"));
            Computer computerL = new Computer("L", InetAddress.getByName("1.2.3.12"));
            Computer computerM = new Computer("M", InetAddress.getByName("1.2.3.13"));
            Computer computerN = new Computer("N", InetAddress.getByName("1.2.3.14"));
            Computer computerO = new Computer("O", InetAddress.getByName("1.2.3.15"));
            Computer computerP = new Computer("P", InetAddress.getByName("1.2.3.16"));
            Computer computerQ = new Computer("Q", InetAddress.getByName("1.2.3.17"));
            Computer computerR = new Computer("R", InetAddress.getByName("1.2.3.18"));
            Computer computerS = new Computer("S", InetAddress.getByName("1.2.3.19"));
            Computer computerT = new Computer("T", InetAddress.getByName("1.2.3.20"));

            // Create a network switch with 4 ports.
            NetworkSwitch networkSwitch = new NetworkSwitch(20);
            
            // This connects network card of ComputerA
            // to Port 0 on the Network Switch.
            SwitchPort port0 = networkSwitch.getPort(0);
            port0.connectNetworkCard((NetworkCard) computerA);
            computerA.connectPort(port0);
            
            // This connects network card of ComputerB
            // to Port 1 on the Network Switch.
            SwitchPort port1 = networkSwitch.getPort(1);
            port1.connectNetworkCard((NetworkCard) computerB);
            computerB.connectPort(port1);
            
            SwitchPort port2 = networkSwitch.getPort(2);
            port2.connectNetworkCard((NetworkCard) computerC);
            computerC.connectPort(port2);
            
            SwitchPort port3 = networkSwitch.getPort(3);
            port3.connectNetworkCard((NetworkCard) computerD);
            computerD.connectPort(port3);
            
            SwitchPort port4 = networkSwitch.getPort(4);
            port4.connectNetworkCard((NetworkCard) computerE);
            computerE.connectPort(port4);
            
            SwitchPort port5 = networkSwitch.getPort(5);
            port5.connectNetworkCard((NetworkCard) computerF);
            computerF.connectPort(port5);
            
            SwitchPort port6 = networkSwitch.getPort(6);
            port6.connectNetworkCard((NetworkCard) computerG);
            computerG.connectPort(port6);
            
            SwitchPort port7 = networkSwitch.getPort(7);
            port7.connectNetworkCard((NetworkCard) computerH);
            computerH.connectPort(port7);
            
            SwitchPort port8 = networkSwitch.getPort(4);
            port8.connectNetworkCard((NetworkCard) computerI);
            computerI.connectPort(port8);
            
            SwitchPort port9 = networkSwitch.getPort(9);
            port9.connectNetworkCard((NetworkCard) computerJ);
            computerJ.connectPort(port9);
            
            SwitchPort port10 = networkSwitch.getPort(10);
            port10.connectNetworkCard((NetworkCard) computerK);
            computerK.connectPort(port10);
            
            SwitchPort port11 = networkSwitch.getPort(11);
            port11.connectNetworkCard((NetworkCard) computerL);
            computerL.connectPort(port11);
            
            SwitchPort port12 = networkSwitch.getPort(12);
            port12.connectNetworkCard((NetworkCard) computerM);
            computerM.connectPort(port12);
            
            SwitchPort port13 = networkSwitch.getPort(13);
            port13.connectNetworkCard((NetworkCard) computerN);
            computerN.connectPort(port13);

            SwitchPort port14 = networkSwitch.getPort(14);
            port14.connectNetworkCard((NetworkCard) computerO);
            computerO.connectPort(port14);
            
            SwitchPort port15 = networkSwitch.getPort(15);
            port15.connectNetworkCard((NetworkCard) computerP);
            computerP.connectPort(port15);
            
            SwitchPort port16 = networkSwitch.getPort(16);
            port16.connectNetworkCard((NetworkCard) computerQ);
            computerQ.connectPort(port16);
            
            SwitchPort port17 = networkSwitch.getPort(17);
            port17.connectNetworkCard((NetworkCard) computerR);
            computerR.connectPort(port17);
            
            SwitchPort port18 = networkSwitch.getPort(18);
            port18.connectNetworkCard((NetworkCard) computerS);
            computerS.connectPort(port18);
            
            SwitchPort port19 = networkSwitch.getPort(19);
            port19.connectNetworkCard((NetworkCard) computerT);
            computerT.connectPort(port19);
            
            // Start the switch operating.
            // Essentially the switch starts forwarding network packets.
            networkSwitch.powerUp();

            // Create a 'ParrotServer' running on Computer B listening to port number 9999.
            Application parrotServer = new ParrotServer((ComputerOS) computerB, 1000);
            parrotServer.start();
            
            Application parrotServer2 = new ParrotServer((ComputerOS) computerD, 1001);
            parrotServer2.start();
            
            Application parrotServer3 = new ParrotServer((ComputerOS) computerF, 1002);
            parrotServer3.start();
            
            Application parrotServer4 = new ParrotServer((ComputerOS) computerH, 1003);
            parrotServer4.start();
            
            Application parrotServer5 = new ParrotServer((ComputerOS) computerJ, 1004);
            parrotServer5.start();
            
            Application parrotServer6 = new ParrotServer((ComputerOS) computerL, 1005);
            parrotServer6.start();
            
            Application parrotServer7 = new ParrotServer((ComputerOS) computerN, 1006);
            parrotServer7.start();
            
            Application parrotServer8 = new ParrotServer((ComputerOS) computerP, 1007);
            parrotServer8.start();
            
            Application parrotServer9 = new ParrotServer((ComputerOS) computerR, 1008);
            parrotServer9.start();
            
            Application parrotServer10 = new ParrotServer((ComputerOS) computerT, 1009);
            parrotServer10.start();
            

            // Create a 'HelloWorldClient' running on 10 computers
            Application helloWorldClient = new HelloWorldClient((ComputerOS) computerA, InetAddress.getByName("1.2.3.2"), 1000);
            helloWorldClient.start();
            
            Application helloWorldClient2 = new HelloWorldClient((ComputerOS) computerC, InetAddress.getByName("1.2.3.4"), 1001);
            helloWorldClient2.start();
            
            Application helloWorldClient3 = new HelloWorldClient((ComputerOS) computerE, InetAddress.getByName("1.2.3.6"), 1002 );
            helloWorldClient3.start();
            
            Application helloWorldClient4 = new HelloWorldClient((ComputerOS) computerG, InetAddress.getByName("1.2.3.8"), 1003);
            helloWorldClient4.start();
            
            Application helloWorldClient5 = new HelloWorldClient((ComputerOS) computerI, InetAddress.getByName("1.2.3.10"), 1004);
            helloWorldClient5.start();
            
            Application helloWorldClient6 = new HelloWorldClient((ComputerOS) computerK, InetAddress.getByName("1.2.3.12"), 1005 );
            helloWorldClient6.start();
            
            Application helloWorldClient7 = new HelloWorldClient((ComputerOS) computerM, InetAddress.getByName("1.2.3.14"), 1006);
            helloWorldClient7.start();
            
            Application helloWorldClient8 = new HelloWorldClient((ComputerOS) computerO, InetAddress.getByName("1.2.3.16"), 1007);
            helloWorldClient8.start();
            
            Application helloWorldClient9 = new HelloWorldClient((ComputerOS) computerQ, InetAddress.getByName("1.2.3.18"), 1008);
            helloWorldClient9.start();
            
            Application helloWorldClient10 = new HelloWorldClient((ComputerOS) computerS, InetAddress.getByName("1.2.3.20"), 1009);
            helloWorldClient10.start();
            

        } catch (UnknownHostException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
}