/*
 *  (c) K.Bryson, Dept. of Computer Science, UCL (2013)
 */
package switched_network;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;

/**
 *
 * Defines a network switch with a number of LAN Ports.
 *
 * @author K. Bryson.
 */
public class NetworkSwitch extends Thread {

    private final SwitchPort[] ports;
    
    /*
     * Create a Network Switch the specified number of LAN Ports.
     */
    public NetworkSwitch(int numberPorts) {
    	
        ports = new SwitchPort[numberPorts];

        // Create each ports.
        for (int i = 0; i < numberPorts; i++) {
            ports[i] = new SwitchPort(i);
        }

    }

    public int getNumberPorts() {
    	
        return ports.length;
        
    }
    
    
    public SwitchPort getPort(int number) {
    	
    	return ports[number];
    	
    }
    
    private int twoByteToInt(byte[] i) {
    	int high = i[1] >= 0 ? i[1] : 256 + i[1];
    	int low = i[0] >= 0 ? i[0] : 256 + i[0];
    	
    	return (low | (high << 8));
    }
    
    private void getDetail(byte[] protocol) {
    	byte[] src = new byte[4];
    	byte[] dest = new byte[4];
    	byte[] portOut = new byte[2];
    	byte[] portIn = new byte[2];
    	
    	System.arraycopy(protocol, 0, src, 0, src.length);
    	System.arraycopy(protocol, src.length, dest, 0, dest.length);
    	System.arraycopy(protocol, src.length + dest.length, portOut, 0, portOut.length);
    	System.arraycopy(protocol, src.length + dest.length + portOut.length, portIn, 0, portIn.length);
    	
    	ByteArrayOutputStream os = new ByteArrayOutputStream();
    	
    	byte[] data = new byte[protocol.length - 12];
    	System.arraycopy(protocol, 12, data, 0, data.length);
    	
    	try {
			os.write(data);
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
    	
    	try {
			System.out.println("Source: " + InetAddress.getByAddress(src));
			System.out.println("Destination: " + InetAddress.getByAddress(dest));
			System.out.println("Outbound port: " + twoByteToInt(portOut));
			System.out.println("Inbound port: " + twoByteToInt(portIn));
			System.out.println("Payload: " + os.toString());
    	} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    
    /*
     * Power up the Network Switch so that it starts
     * processing/forwarding network packet traffic.
     */
    public void powerUp() {
    	// start thread
    	this.start();
    }
    
    private byte[] getSource(byte[] protocol) {
    	byte[] data = new byte[4];
    	System.arraycopy(protocol, 0, data, 0, data.length);
    	
    	return data;
    }
    
    private byte[] getDestination(byte[] protocol) {
    	byte[] data = new byte[4];
    	
    	System.arraycopy(protocol, 4, data, 0, data.length);
    	
    	return data;
    }
    
    // go through each ports for incoming packets
    private byte[] getPacket() throws InterruptedException {
    	int i = 0;
    	byte[] packet = null;
    	while(true) {
    		if (i == getNumberPorts()) {
    			i = 0;
    		}
    		packet = this.ports[i].getIncomingPacket();
    		if (packet != null) {
    			//System.out.println("Packet incoming...");
    			return packet;
    		}
    		i++;
    	}
    }
    
    // allocate packet to specific switch port
    private void allocatePacket(byte[] packet, byte[] destination) throws InterruptedException {
    	int i = 0;
    	while(true) {
    		if ( i == getNumberPorts()) {
    			break;
    		}
    		try {
    			if (Arrays.equals(this.ports[i].getIPAddress().getAddress(), destination) == true) {
    				this.ports[i].sendToComputer(packet);
    				break;
    			}
    		} catch(NullPointerException e) {
    			
    		}
    		i++;
    	}
    }
    
    // handling packets in network switch
	private void processNetwork() throws IOException, InterruptedException {
			byte[] packet = getPacket();
			
			//System.out.println("Network Switch received: " + new String(packet));
			byte[] dest = getDestination(packet);
			allocatePacket(packet, dest);
	}
 
    
    // This thread is responsible for delivering any current incoming packets.
    public void run() {
    	
    	while(true) {
    
			try {
				processNetwork();
			} catch (IOException | InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
    }
}

