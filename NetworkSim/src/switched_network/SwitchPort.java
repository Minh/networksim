/*
 *  (c) K.Bryson, Dept. of Computer Science, UCL (2013)
 */

package switched_network;

import java.net.InetAddress;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;


/**
 *
 * This represents a 'Switch Port' on the front of a Network Switch.
 *
 * For instance, a Network Switch may have 4 ports which are
 * Ethernet sockets at the front of the Network Switch.
 * Each Ethernet socket is physically connected to the
 * network card of a computer using an Ethernet cable.
 *
 * @author K. Bryson.
 */
public class SwitchPort {

    private final int portNumber;
    private NetworkCard connectedNetworkCard = null;
    private InetAddress ipAddress = null;
    
    private BlockingQueue<byte[]> queue = new ArrayBlockingQueue<byte[]>(10);
    
    public SwitchPort(int number) {
        portNumber = number;
    }
    
    public int getNumber() {
    	return portNumber;
    }
    
    public InetAddress getIPAddress() {
    	return ipAddress;
    }

    /*
     * This method is USED BY THE COMPUTER to send a packet of
     * data to this Port on the Switch.
     *
     * The packet of data should follow the simplified
     * header format as specified in the coursework descriptions.
     */
    
    public void sendToNetwork(byte[] packet) throws InterruptedException {

    	// YOU NEED TO IMPLEMENT THIS METHOD.
		queue.put(packet);
    }

    /*
     * Method get used NetworkSwitch
     */
    
    public byte[] getIncomingPacket() throws InterruptedException {

    	if (queue.peek() != null) {
    		byte[] packet = queue.take();
    		//System.out.println("Switch releasing packet: " + packet);
    		return packet;
    	}
    	return null;
    }
    
    void sendToComputer(byte[] packet) throws InterruptedException {

    	// YOU NEED TO IMPLEMENT THIS METHOD.
    	try {
    		connectedNetworkCard.sendToComputer(packet);
    	} catch(NullPointerException e) {
    		
    	}
    }

    void connectNetworkCard(NetworkCard networkCard) {

        connectedNetworkCard = networkCard;
        ipAddress = networkCard.getIPAddress();
 
    }

}
