/*
 *  (c) K.Bryson, Dept. of Computer Science, UCL (2013)
 */

package switched_network;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Arrays;

/**
 * Models a computer system which can have a number of
 * applications running on it and provides these applications
 * with operating system services including networking.
 *
 * The Computer provides the operating system services by
 * implementing the ComputerOS interface.
 *
 * The Computer also handles network traffic to/from switch ports
 * by implementing a NetworkCard interface.
 *
 * @author K. Bryson.
 */
public class Computer implements ComputerOS, NetworkCard {

    private final String hostname;
    private final InetAddress ipAddress;

    // This is the switch port which the computer is attached to.
    private SwitchPort port = null;

    private final static int MAX_PORTS = 65536;
    
    private byte[] packet = null;
    
    public Computer(String hostname, InetAddress ipAddress) {
    	
        this.hostname = hostname;
        this.ipAddress = ipAddress;

    }

    
    
    /**********************************************************************************
     * The following methods provide the 'Operating System Services'
     * of the computer which are directly used by applications.
     **********************************************************************************/

    /*
     * Get the host name of the computer.
     */
    public String getHostname() {
        return hostname;
    }
    
    /**
     * Convert int to 2 bytes
     * @param i Integer to be converted
     * @return
     */
    private byte[] intToTwoByte(int i) {
    	byte[] data = new byte[2];
    	data[0] = (byte) (i & 0xFF);
    	data[1] = (byte) ((i >> 8) & 0xFF);
    	return data;
    }
    
    /**
     * Converting two bit back to integer
     * @param i
     * @return
     */
    private int twoByteToInt(byte[] i) {
    	int high = i[1] >= 0 ? i[1] : 256 + i[1];
    	int low = i[0] >= 0 ? i[0] : 256 + i[0];
    	
    	return (low | (high << 8));
    }
    
    /**
     * Get protocol details
     * @param protocol
     */
    private void getDetail(byte[] protocol) {
    	byte[] src = new byte[4];
    	byte[] dest = new byte[4];
    	byte[] portOut = new byte[2];
    	byte[] portIn = new byte[2];
    	
    	System.arraycopy(protocol, 0, src, 0, src.length);
    	System.arraycopy(protocol, src.length, dest, 0, dest.length);
    	System.arraycopy(protocol, src.length + dest.length, portOut, 0, portOut.length);
    	System.arraycopy(protocol, src.length + dest.length + portOut.length, portIn, 0, portIn.length);
    	
    	ByteArrayOutputStream os = new ByteArrayOutputStream();
    	
    	for(int i=12;i < protocol.length;i++) {
    		byte[] data = new byte[1];
    		data[0] = protocol[i];
    		try {
				os.write(data);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    	}
    	
    	try {
			System.out.println("Source: " + InetAddress.getByAddress(src));
			System.out.println("Destination: " + InetAddress.getByAddress(dest));
			System.out.println("Outbound port: " + twoByteToInt(portOut));
			System.out.println("Inbound port: " + twoByteToInt(portIn));
			System.out.println("Payload: " + byteToString(os.toByteArray()));
    	} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    
    // convert byte[] to string
    private String byteToString(byte[] data) throws IOException {
    	ByteArrayOutputStream os = new ByteArrayOutputStream();
    	os.write(data);
    	return os.toString();
    }
    
    /**
     * Generate protocol
     * @param payload
     * @param ip_address_to
     * @param port_from
     * @param port_to
     * @return
     * @throws IOException 
     */
    private byte[] genProtocol(byte[] payload, InetAddress ip_address_to, int port_from, int port_to)  {
    	ByteArrayOutputStream os = new ByteArrayOutputStream();
    	
    	byte[] outboundPort = intToTwoByte(port_from);
    	byte[] inboundPort = intToTwoByte(port_to);
    	
    	try {
			// pad payload if necessary
			os.write(this.ipAddress.getAddress());
			os.write(ip_address_to.getAddress());
			os.write(outboundPort);
			os.write(inboundPort);
			os.write(payload);
    	} catch (IOException e) {
    		e.printStackTrace();
    	}
    	
    	return os.toByteArray();
    }
    
    /*
     * Ask the operating system to send this message
     * (byte array as the payload) to the computer with
     * the given IP Address.
     *
     * The message goes from a 'port' on this machine to
     * the specified port on the other machine.
     */
    public void send(byte[] payload, InetAddress ip_address_to, int port_from, int port_to) {

    	// YOU NEED TO IMPLEMENT THIS METHOD.
    	byte[] packet = genProtocol(payload, ip_address_to, port_from, port_to);
    	//getDetail(packet);
    	
    	try {
			this.port.sendToNetwork(packet);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NullPointerException e) {
			
		}
    }
    
    // check port
    private boolean checkPort(int port, byte[] packet) {
    	byte[] portIn = new byte[2];
    	System.arraycopy(packet, 10, portIn, 0, 2);
    	
    	if (twoByteToInt(portIn) == port) {
    		return true;
    	}
    	
    	return false;
    }
    
    // get payload from packet
    private byte[] getPayload(byte[] packet) {
    	
    	byte[] data = new byte[packet.length - 12];
    	System.arraycopy(packet, 12, data, 0, data.length);

    	return data;
    }
    
    /*
     * This asks the operating system to check whether any incoming messages
     * have been received on the given port on this machine.
     *
     * If a message is pending then the 'payload' is returned as a byte array.
     * (i.e. without any UDP/IP header information)
     */
    public byte[] recv(int port) {
    	
    	// YOU NEED TO IMPLEMENT THIS METHOD.
    	if (this.packet != null) {
    		if (checkPort(port, this.packet) == true) {
    			byte[] packet = getPayload(this.packet);
    			this.packet = null;
    			return packet;
    		}
    	}
    	return null;
    	
    	

    }


    /**********************************************************************************
     * The following methods implement the Network Card interface for this computer.
     *
     * They are used by the 'operating system' to send and recv packets.
     ***********************************************************************************/


    /*
     * Get the IP Address of the network card.
     */
    public InetAddress getIPAddress() {
        return ipAddress;
    }

    
    /*
     * This allows a port of a network switch
     * to be attached to this computers network card.
     */
    public void connectPort(SwitchPort port) {
        this.port = port;
    }
    

    /*
     * This method is used by the Network Switch (SwitchPort)
     * to send a packet of data from the network to this computer.
     */
    public void sendToComputer(byte[] packet) {
    	
		byte[] dest = new byte[4];

		// getDetail(packet);

		// get destination port
		System.arraycopy(packet, 4, dest, 0, 4);
		//System.out.println("Destination ip: " + InetAddress.getByAddress(dest));

		if (Arrays.equals(dest, this.ipAddress.getAddress()) == true) {
			//System.out.println(this.hostname + " received: " + new String(packet));
			this.packet = packet;
		}
    }
}
